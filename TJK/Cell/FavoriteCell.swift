//
//  FavoriteCell.swift
//  TJK
//
//  Created by Shatlyk on 01.07.2022.
//

import UIKit

class FavoriteCell: UICollectionViewCell {
    
    var removePressed : (() -> ()) = {}
    let basketImage = UIImage(named: "basket")
    
    @IBOutlet weak var fImageVeiw: UIImageView!
    @IBOutlet weak var removeFavoriteButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var basketButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //customize UI
        let tintedImage = basketImage?.withRenderingMode(.alwaysTemplate)
        basketButton.setImage(tintedImage, for: .normal)
        basketButton.tintColor = .white
    }
    @IBAction func removeTap(_ sender: Any) {
        removePressed()
    }
}
