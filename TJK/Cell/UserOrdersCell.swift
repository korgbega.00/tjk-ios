//
//  UserOrdersCell.swift
//  TJK
//
//  Created by Shatlyk on 19.09.2022.
//

import UIKit

class UserOrdersCell: UITableViewCell {

    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
