//
//  DetailCell.swift
//  TJK
//
//  Created by Shatlyk on 28.06.2022.
//

import UIKit

class DetailCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
 
    override  func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
