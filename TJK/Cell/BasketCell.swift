//
//  BasketCell.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class BasketCell: UITableViewCell {

    var plusButtonPressed : (() -> ()) = {}
    var minusButtonPressed : (() -> ()) = {}

    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var myImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    @IBAction func plusButtonTap(_ sender: Any) {
        plusButtonPressed()
    }
    @IBAction func minusButtonTap(_ sender: Any) {
        minusButtonPressed()
    }
    
}
