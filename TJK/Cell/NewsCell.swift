//
//  TableViewCell.swift
//  TJK
//
//  Created by Shatlyk on 29.06.2022.
//

import UIKit

class NewsCell: UITableViewCell {
        
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
}
