//
//  PopularGoodsCell.swift
//  TJK
//
//  Created by Shatlyk on 21.06.2022.
//

import UIKit

class PopularGoodsCell: UICollectionViewCell {
    
    let basketImage = UIImage(named: "basket")
    var addFavoritePressed : (() -> ()) = {}
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var basketButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //customize UI
        let tintedImage = basketImage?.withRenderingMode(.alwaysTemplate)
        basketButton.setImage(tintedImage, for: .normal)
        basketButton.tintColor = .white
        imageView.backgroundColor = .lightGray

    }
    @IBAction func addFavorite(_ sender: Any) {
        addFavoritePressed()
    }
    
}
