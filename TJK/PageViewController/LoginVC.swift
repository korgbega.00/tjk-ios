//
//  LoginVC.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class LoginVC: UIViewController {
    
    let apiCall = ApiCall()
    let helperDB = HelperDB()
    var myTabBarController = TabBarController()
    var customStoryboard = UIStoryboard()
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        //init
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        myTabBarController = customStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
       
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        guard let token = helperDB.getToken()?.access else { return }
        
        apiCall.getRequestAuth(url: "https://tjk.com.tm:8000/api/user", token: token ) { tokenJson in
            guard let json2 = tokenJson else { print("json2 = nil"); return }
            print(json2)
            
            do {
            let user: LoginResponse = try JSONDecoder().decode(LoginResponse.self, from: json2)
                print(user)
                
            } catch (let error2) {
                print(error2)
            }
        
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginButtonTap(_ sender: Any) {
        guard let emailText = emailTextField.text else { toast(); return }
        guard let passwordText = passwordTextField.text else { toast(); return }
    
        if (emailText == "") || (passwordText == "") {
            self.toast()
            return
        }
        
        
        let param : [String : Any] = [
            "username" : emailText,
            "password" : passwordText]
        
        //first request
        self.apiCall.postRequest(url: URLString.URL_TOKEN, param: param) { tokenJson in
            guard let json2 = tokenJson else { print("json2 = nil"); return }
            
            do {
            let token: Token = try JSONDecoder().decode(Token.self, from: json2)
                print("token = \(token)")
                
                //save token
                let myToken = MyToken(refresh: token.refresh , access: token.access)
                
                do {
                    let encoder = JSONEncoder()
                    let data2 = try encoder.encode(myToken)
                    self.defaults.set(data2, forKey: "MyToken")
                    
                    guard let _ = self.helperDB.getToken() else {
                        self.toastSecond()
                        return }
                    
                    //second request
                    self.apiCall.getRequestAuth(url: "https://tjk.com.tm:8000/api/user", token: token.access ) { tokenJson in
                        guard let json2 = tokenJson else { print("json2 = nil"); return }
                        
                        do {
                        let loginResponse: LoginResponse = try JSONDecoder().decode(LoginResponse.self, from: json2)
                            
                            let userDB = UserDB(id: loginResponse.user.id, username: loginResponse.user.username, email: loginResponse.user.email, account: AccountDB(id: loginResponse.user.account.id, address: loginResponse.user.account.address, phone: loginResponse.user.account.phone), first_name: loginResponse.user.first_name, last_name: loginResponse.user.last_name)
                            
                            do {
                                let encoder = JSONEncoder()
                                let data = try encoder.encode(userDB)
                                self.defaults.set(data, forKey: "User")
                                
                            } catch {
                                print(error)
                                self.toastSecond()

                            }
                            self.view.window?.rootViewController = self.myTabBarController

                            
                        } catch (let error2) {
                            print(error2)
                            self.toastSecond()
                        }
                    
                    }
                    
                } catch {
                    print(error)
                    self.toastSecond()
                }
                
            } catch (let error2) {
                print(error2)
                self.toastSecond()
            }
        }
    }
    
    
    private func toast(){
        if currentLanguage == "tk" {
            self.showToast(message: "Ählisini dolduryň", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "ru" {
            self.showToast(message: "Заполните все поля", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "en" {
            self.showToast(message: "Fill in all", font: .systemFont(ofSize: 14.0))
        }
        
    }
    
    private func toastSecond(){
        if currentLanguage == "tk" {
            self.showToast(message: "Başa barmady", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "ru" {
            self.showToast(message: "Не получилось", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "en" {
            self.showToast(message: "Did not work out", font: .systemFont(ofSize: 14.0))
        }
    }
}


extension LoginVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            
            if textField == emailTextField {
                if  newString.count > 50 {
                    return false
                }
            }
            
            if textField == passwordTextField {
                if  newString.count > 20 {
                    return false
                }
            }
            
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
