//
//  ProfilPVC.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class ProfilPVC: UIPageViewController {

    var customStoryboard = UIStoryboard()
    var loginVC = UIViewController()
    var registerVC = UIViewController()
    var vcArray = [UIViewController]()
    var indexCurrentVC = 0
    weak var profilVCDelegate: ProfilVCDelegate?
    let helperDB = HelperDB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        loginVC = customStoryboard.instantiateViewController(withIdentifier: "LoginVC")
        registerVC = customStoryboard.instantiateViewController(withIdentifier: "RegisterVC")

        vcArray.append(loginVC)
        vcArray.append(registerVC)
        
        if helperDB.getUser() != nil {
            let userVC = customStoryboard.instantiateViewController(withIdentifier: "UserVC") as! UserVC
            self.setViewControllers([userVC], direction: .forward, animated: true, completion: nil)
        } else {
            self.dataSource = self
            self.setViewControllers([vcArray[0]], direction: .forward, animated: true, completion: nil)

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    public func setVC(index: Int) {
        if indexCurrentVC == 0 {
            self.setViewControllers([vcArray[index]], direction: .forward, animated: true, completion: nil)
            indexCurrentVC = index
        }
        if indexCurrentVC == 1 {
            self.setViewControllers([vcArray[index]], direction: .reverse, animated: true, completion: nil)
            indexCurrentVC = index
        }
    }
}

extension ProfilPVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        profilVCDelegate?.changeConsZero()
        if let index = vcArray.firstIndex(of: viewController) {
            if index == 1 {
                return vcArray[0]
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        profilVCDelegate?.changeConsOne()
        if let index = vcArray.firstIndex(of: viewController) {
            if index == 0 {
                return vcArray[1]
            }
        }
        return nil
    }


}
