//
//  MainPVC.swift
//  TJK
//
//  Created by Shatlyk on 18.06.2022.
//

import UIKit

class MainPVC: UIPageViewController {
    
    var viewControllerArray = [UIViewController]()
    var slugArray = [String]()
    var indexCurrentVC = 0
    let apiCall = ApiCall()
    var customStryboard = UIStoryboard()
    weak var mainVCDelegate: MainVCDelegate?
    var currentLanguage = "ru"
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        customStryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        call()

    }
    
    private func call(){
        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        apiCall.getRequest(url: URLString.URL_CATEGORIES) { resultJson in
            guard let resultJson = resultJson else {
                self.mainVCDelegate?.noNetIt(true)
                print("resultJson = nil")
                return
                
            }
            
            self.mainVCDelegate?.noNetIt(false)
            
            let categiroResponse: CategoriResponse = try! JSONDecoder().decode(CategoriResponse.self, from: resultJson)
            for i in (0 ..< (categiroResponse.count)) {
                self.slugArray.append(categiroResponse.results[i].slug)
                
                let vc = self.customStryboard.instantiateViewController(withIdentifier: "PopularGoodsViewController") as! PopularGoodsVC
                vc.slug = self.slugArray[i]
            
                if self.currentLanguage == "tk" {
                    self.mainVCDelegate?.changeButtonTitle(index: i, title: categiroResponse.results[i].title)
                } else if self.currentLanguage == "ru" {
                    self.mainVCDelegate?.changeButtonTitle(index: i, title: categiroResponse.results[i].title_ru)
                } else if self.currentLanguage == "en" {
                    self.mainVCDelegate?.changeButtonTitle(index: i, title: categiroResponse.results[i].title_en)
                }
                
                
                self.viewControllerArray.append(vc)
        
                self.setViewControllers([self.viewControllerArray[0]] , direction: .forward, animated: true, completion: nil)
                
            }
        }
    }
    
    public func setVC(index: Int) {
        if (indexCurrentVC < index) && (self.viewControllerArray.count > index) {
            self.setViewControllers([viewControllerArray[index]], direction: .forward, animated: true, completion: nil)
            indexCurrentVC = index
        }
        if (indexCurrentVC > index ) && (self.viewControllerArray.count > index) {
            self.setViewControllers([viewControllerArray[index]], direction: .reverse, animated: true, completion: nil)
            indexCurrentVC = index
        }
    }
}
