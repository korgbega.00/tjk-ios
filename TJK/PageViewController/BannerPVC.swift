//
//  BannerPVC.swift
//  TJK
//
//  Created by Shatlyk on 26.06.2022.
//

import UIKit

class BannerPVC: UIPageViewController {

    var viewControllerArray = [UIViewController]()
    var imageArray = [UIImage]()
    var indexPager = 0
    var timer = Timer()
    let apiCall = ApiCall()
    var customStoryboard = UIStoryboard()
    var currentLanguage = "ru"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        dataSource = self
                
        call()

    }
    
    private func call() {
        apiCall.getRequest(url: URLString.URL_SLIDES) {
            resultJson in
            guard let resultJson = resultJson else { print("resultJson = nil"); return }
            
            let slideResponse: SlidesResponse = try! JSONDecoder().decode(SlidesResponse.self, from: resultJson)
            print("slideResponse.count = \(slideResponse.count)")
            
            if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
                self.currentLanguage = language
            }
                        
            if self.currentLanguage == "tk" {
                for i in (0 ..< (slideResponse.count)) {
                    self.apiCall.getData(from: URL(string: slideResponse.results[i].image)!) { data, response, error in
                            guard let data = data, error == nil else { return }
                        print(response?.suggestedFilename as Any)
                            DispatchQueue.main.async() {
                                self.imageArray.append(UIImage(data: data)!)
                                let vc = self.customStoryboard.instantiateViewController(withIdentifier: "BannerVC") as! BannerVC
                                vc.image = UIImage(data: data)!
                                self.viewControllerArray.append(vc)
                                
                                if i == (slideResponse.count - 1) {
                                    self.setViewControllers([self.viewControllerArray[0]] , direction: .forward, animated: true, completion: nil)
                                    self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.TimerFunc), userInfo: nil, repeats: true)
                                }
                            }
                    }
                }
                
            } else if self.currentLanguage == "ru" {
                for i in (0 ..< (slideResponse.count)) {
                    self.apiCall.getData(from: URL(string: slideResponse.results[i].image_ru)!) { data, response, error in
                            guard let data = data, error == nil else { return }
                        print(response?.suggestedFilename as Any)
                            DispatchQueue.main.async() {
                                self.imageArray.append(UIImage(data: data)!)
                                let vc = self.customStoryboard.instantiateViewController(withIdentifier: "BannerVC") as! BannerVC
                                vc.image = UIImage(data: data)!
                                self.viewControllerArray.append(vc)
                                
                                if i == (slideResponse.count - 1) {
                                    self.setViewControllers([self.viewControllerArray[0]] , direction: .forward, animated: true, completion: nil)
                                    self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.TimerFunc), userInfo: nil, repeats: true)
                                    
                                }
                            }
                    }
                }
                
            } else if self.currentLanguage == "en" {
                for i in (0 ..< (slideResponse.count)) {
                    self.apiCall.getData(from: URL(string: slideResponse.results[i].image_en)!) { data, response, error in
                            guard let data = data, error == nil else { return }
                        print(response?.suggestedFilename as Any)
                            DispatchQueue.main.async() {
                                self.imageArray.append(UIImage(data: data)!)
                                let vc = self.customStoryboard.instantiateViewController(withIdentifier: "BannerVC") as! BannerVC
                                vc.image = UIImage(data: data)!
                                self.viewControllerArray.append(vc)
                                
                                if i == (slideResponse.count - 1) {
                                    self.setViewControllers([self.viewControllerArray[0]] , direction: .forward, animated: true, completion: nil)
                                    self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.TimerFunc), userInfo: nil, repeats: true)
                                }
                            }
                    }
                }
            }
        }
    }
    
    @objc func TimerFunc(){
        setViewControllers([viewControllerArray[indexPager]], direction: .forward, animated: true)
        print("\(indexPager)")
        if indexPager + 1 == imageArray.count {
            indexPager = 0
        } else {
            indexPager += 1
        }
    }
}

extension BannerPVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? BannerVC else { return nil }
        if let index  = viewControllerArray.firstIndex(of: viewController) {
            if index > 0 {
                return viewControllerArray[index - 1]
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? BannerVC else { return nil }
        if let index  = viewControllerArray.firstIndex(of: viewController) {
            if index < self.viewControllerArray.count - 1 {
                return viewControllerArray[index + 1]
            }
        }
        return nil
    }
}
