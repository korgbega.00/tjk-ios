//
//  BannerPopularGoodsDetailPVC.swift
//  TJK
//
//  Created by Shatlyk on 08.07.2022.
//

import UIKit

class BannerPopularGoodsDetailPVC: UIPageViewController {

    var productResultsImagesArray = [ProductResultsImages]()
    var viewControllerArray = [DetailBannerVC]()
    var customStoryboard = UIStoryboard()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        for i in (0 ..< productResultsImagesArray.count) {
            let vc = customStoryboard.instantiateViewController(withIdentifier: "DetailBannerVC") as! DetailBannerVC
            vc.urlImage = productResultsImagesArray[i].image
            viewControllerArray.append(vc)
            
            if i == productResultsImagesArray.count - 1 {
                setViewControllers([viewControllerArray[0]], direction: .forward, animated: true, completion: nil)
            }
        }
        
        dataSource = self

    }
}
extension BannerPopularGoodsDetailPVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? DetailBannerVC else { return nil }
        if let index  = viewControllerArray.firstIndex(of: viewController) {
            if index > 0 {
                return viewControllerArray[index - 1]
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? DetailBannerVC else { return nil }
        if let index  = viewControllerArray.firstIndex(of: viewController) {
            if index < self.viewControllerArray.count - 1 {
                return viewControllerArray[index + 1]
            }
        }
        return nil
    }
}
