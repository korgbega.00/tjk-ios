//
//  MyToken.swift
//  TJK
//
//  Created by Shatlyk on 13.07.2022.
//

import Foundation

struct MyToken: Codable {
    
    let refresh: String
    let access: String
}
