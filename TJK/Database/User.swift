//
//  User.swift
//  TJK
//
//  Created by Shatlyk on 12.07.2022.
//

import Foundation

struct UserDB: Codable {
    let id: Int
    let username: String
    let email: String
    let account: AccountDB?
    let first_name: String
    let last_name: String
}

struct AccountDB: Codable {
    let id: Int
    let address: String
    let phone: String
}
