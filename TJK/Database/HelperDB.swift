//
//  HelperDB.swift
//  TJK
//
//  Created by Shatlyk on 10.07.2022.
//

import Foundation

class HelperDB {
    
    public func getOrders() -> [Order]{
        let defaults = UserDefaults.standard
        
        if let data = defaults.data(forKey: "Orders") {
            do {
                let decoder = JSONDecoder()
                let orders = try decoder.decode([Order].self, from: data)
            
                return orders
                
            } catch {
                print(error)
            }
        }
        return [Order]()
    }
    
    public func setOrders(_ orders: [Order]) {
        let defaults = UserDefaults.standard

        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(orders)
            defaults.set(data, forKey: "Orders")
            
        } catch {
            print(error)
        }
    }
    
    public func addOrder(_ order: Order) {
        var orders = getOrders()
        
        for i in (0 ..< (orders.count)) {
            if order.id == orders[i].id {
                orders[i].count = order.count
                orders[i].size = order.size
                setOrders(orders)
                return
            }
        }
        orders.append(order)
        setOrders(orders)
    }
    
    public func removeOrder(index: Int) {
        let defaults = UserDefaults.standard
        var orders = getOrders()
        orders.remove(at: index)
        
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(orders)
            defaults.set(data, forKey: "Orders")
            
        } catch {
            print(error)
        }
    }
    
    
    public func getUser() -> UserDB? {
        let defaults = UserDefaults.standard
        
        if let data = defaults.data(forKey: "User") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(UserDB.self, from: data)
            
                return user
                
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    public func removeUser() {
        let defaults = UserDefaults.standard
        
        defaults.removeObject(forKey: "User")
        
    }
    
    public func getToken() -> MyToken? {
        let defaults = UserDefaults.standard
        
        if let data = defaults.data(forKey: "MyToken") {
            do {
                let decoder = JSONDecoder()
                let token = try decoder.decode(MyToken.self, from: data)
            
                return token
                
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    public func removeToken() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "MyToken")
        
    }
    
    //MARK: FAVORITE
    public func setFavorite(_ favorite: [Favorite]) {
        let defaults = UserDefaults.standard

        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(favorite)
            defaults.set(data, forKey: "Favorite")
            
        } catch {
            print(error)
        }
    }
    
    public func getFavorite() -> [Favorite]{
        let defaults = UserDefaults.standard
        
        if let data = defaults.data(forKey: "Favorite") {
            do {
                let decoder = JSONDecoder()
                let favorite = try decoder.decode([Favorite].self, from: data)
            
                return favorite
                
            } catch {
                print(error)
            }
        }
        return [Favorite]()
    }
    
    public func addFavorite(_ favorite: Favorite) {
        var favorites = getFavorite()
        
        for i in (0 ..< (favorites.count)) {
            if favorite.id == favorites[i].id {
                return
            }
        }
        favorites.append(favorite)
        setFavorite(favorites)
    }
}
