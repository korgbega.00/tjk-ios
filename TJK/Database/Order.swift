//
//  Orders.swift
//  TJK
//
//  Created by Shatlyk on 10.07.2022.
//

import Foundation

struct Order: Codable {
    
    let id: Int
    var count: Int
    var size: String
    var image: String
    var name: String
    var name_ru: String
    var name_en: String
    var price: Float
    
}
