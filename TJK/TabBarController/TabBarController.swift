//
//  TabBarController.swift
//  TJK
//
//  Created by Shatlyk on 13.07.2022.
//

import UIKit

class TabBarController: UITabBarController {
    
    var mainStoryboard = UIStoryboard()
    var mainVC = MainVC()
    var favoriteVC = FavoriteVC()
    var basketVC = BasketVC()
    var profileVC = ProfilVC()
    var newsVC = NewsVC()
    let helperDB = HelperDB()
    var currentLanguage = "ru"
    
    let mainImage = UIImage(named: "mainpage_white")
    let mainSelectedImage = UIImage(named: "main_page")
    let favoriteImage = UIImage(named: "heart")
    let favoriteSelectedImage = UIImage(named: "heart_black")
    let basketImage = UIImage(named: "basket")
    let basketSelectedImage = UIImage(named: "basket_black")
    let profileImage = UIImage(named: "profile")
    let profileSelectedImage = UIImage(named: "person_black")
    let newsImage = UIImage(named: "news")
    let newsSelectedImage = UIImage(named: "news_black")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        mainVC = mainStoryboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        favoriteVC = mainStoryboard.instantiateViewController(withIdentifier: "FavoriteVC") as! FavoriteVC
        basketVC = mainStoryboard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfilVC") as! ProfilVC
        newsVC = mainStoryboard.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
        
        if (helperDB.getUser() != nil) {
            self.setViewControllers([mainVC, favoriteVC, basketVC, profileVC, newsVC], animated: true)
            self.tabBar.items![0].image = mainImage
            self.tabBar.items![0].selectedImage = mainSelectedImage
            self.tabBar.items![1].image = favoriteImage
            self.tabBar.items![1].selectedImage = favoriteSelectedImage
            self.tabBar.items![2].image = basketImage
            self.tabBar.items![2].selectedImage = basketSelectedImage
            self.tabBar.items![3].image = profileImage
            self.tabBar.items![3].selectedImage = profileSelectedImage
            self.tabBar.items![4].image = newsImage
            self.tabBar.items![4].selectedImage = newsSelectedImage
            
            if currentLanguage == "tk" {
                self.tabBar.items![1].title = "Saýlanlarym"
                
            } else if currentLanguage == "ru" {
                self.tabBar.items![1].title = "Изобранные"

            } else if currentLanguage == "en" {
                self.tabBar.items![1].title = "Favorite"
            }
            
        } else {
            
            self.setViewControllers([mainVC, basketVC, profileVC, newsVC], animated: true)
            self.tabBar.items![0].image = mainImage
            self.tabBar.items![0].selectedImage = mainSelectedImage
            self.tabBar.items![1].image = basketImage
            self.tabBar.items![1].selectedImage = basketSelectedImage
            self.tabBar.items![2].image = profileImage
            self.tabBar.items![2].selectedImage = profileSelectedImage
            self.tabBar.items![3].image = newsImage
            self.tabBar.items![3].selectedImage = newsSelectedImage
        }
       
        
    }

}
