//
//  UrlString.swift
//  TJK
//
//  Created by Shatlyk on 06.07.2022.
//

import Foundation

let URL_BASE = "https://tjk.com.tm:8000/api/"

class URLString {
    
    static let URL_SLIDES = URL_BASE + "slides/"
    static let URL_NEWS = URL_BASE + "news/tazeliks/"
    static let URL_CATEGORIES = URL_BASE + "products/categories/"
    static let URL_PRODUCTS = URL_BASE + "products/products/"
    static let URL_REGISTER = URL_BASE + "register"    
    static let URL_TOKEN = URL_BASE + "token/"
    static let URL_CREATE_ORDER = URL_BASE + "createorder"
    static let URL_GET_FAVORITE = URL_BASE + "getfavorite/"
    static let URL_ADD_FAVORITE = URL_BASE + "addfavorite"
    static let URL_PRODUCTS_SEARCH = URL_BASE + "products/products"
    static let URL_ABOUT = URL_BASE + "other/about"
    static let MY_ORDERS = URL_BASE + "orders"
    static let DELETE_USER = URL_BASE + "user/delete"



}
