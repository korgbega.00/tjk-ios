//
//  Response.swift
//  TJK
//
//  Created by Shatlyk on 07.07.2022.
//

import Foundation

//MARK: slides
struct SlidesResponse: Decodable {
    
    let count: Int
    let results: [Results]
    
    struct Results: Decodable {
        let image: String
        let image_ru: String
        let image_en: String
    }
}
//MARK: news
struct NewsResponse: Decodable {
    let count: Int
    var results: [NewsResults]
    
}

struct NewsResults: Decodable {
    let id: Int
    let title: String
    let title_ru: String
    let title_en: String
    var description: String
    var description_ru: String
    var description_en: String
    let image: String
    let created_at: String
    let view: Int
}

//MARK: categories
struct CategoriResponse: Decodable {
    let count: Int
    var results: [CategoriResults]
    
}

struct CategoriResults: Decodable {
    let id: Int
    let title: String
    let title_ru: String
    let title_en: String
    let slug: String

}

//MARK: products
struct ProductResponse: Decodable {
    let count: Int
    var results: [ProductResults]
    
}
struct ProductResults: Decodable {
    let id: Int
    let title: String
    let title_ru: String
    let title_en: String
    let price: Float
    let category: ProductResultsCategory
    let sizes: [ProductResultsSizes]
    let images: [ProductResultsImages]
}
struct ProductResultsCategory: Decodable {
    let slug: String
    
}
struct ProductResultsSizes: Decodable {
    let title: String
    let value: Int
}
struct ProductResultsImages: Decodable {
    let image: String
}

//MARK: register
struct RegisterResponse: Decodable {
    let success: String
    var user: User
    
}

struct User: Decodable {
    let id: Int
    let username: String
    let email: String
    let account: Account
    let first_name: String
    let last_name: String
}

struct Account: Decodable {
    let id: Int
    let address: String
    let phone: String
}

//MARK: token
struct Token: Decodable {
    let refresh: String
    let access: String
}

struct LoginResponse: Decodable {
    let user: UserLogin
}

struct UserLogin: Decodable {
    let id: Int
    let username: String
    let email: String
    let first_name: String
    let last_name: String
    let account: Account
}

//MARK: create order
struct OrderResponseCard: Decodable {
    let status: String
    let url: String
}

struct OrderResponse: Decodable {
    let status: String
}

//MARK: get favorite
struct GetFavoriteResponse: Decodable {
    var count: Int
    var results: [GetFavoriteResponseResults]
    
}
struct GetFavoriteResponseResults: Decodable {
    let product: ProductResults
}

//MARK: Status
struct Status: Decodable {
    let status: String
}

//MARK: About Us
struct AboutUsResponse: Decodable {
    let count: Int
    var results: [AboutUsResponseResults]

}

struct AboutUsResponseResults: Decodable {
    let id: Int
    let page: String
    let title: String?
    let title_ru: String?
    let title_en: String?
    let description: String
    let description_ru: String
    let description_en: String
    let image: String?
    let image1: String?
}

//MARK: My Orders
struct MyOrdersResponse: Decodable{
    let count: Int
    let results: [MyOrdersResults]
}
 
struct MyOrdersResults: Decodable {
    let id: Int
    let shipping_method: String
    let total: String
    let created: String
//    let items: [MyOrdersItem]
}

//struct MyOrdersItem: Decodable {
//    let id: Int
//    let amount: Int
//    let size: String
//    let product: [MyOrdesProduct]
//}

//struct MyOrdesProduct: Decodable {
//    let title: String
//    let title_en: String
//    let title_ru: String
////    let price: Float
//    let images: [MyOrdersImage]
//
//}
//
//struct MyOrdersImage: Decodable {
//    let image: String
//}
