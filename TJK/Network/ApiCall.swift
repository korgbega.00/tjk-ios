//
//  Helper.swift
//  TJK
//
//  Created by Shatlyk on 06.07.2022.
//

import Foundation
import Alamofire

class ApiCall {
    public func getRequest(url: String, completion: @escaping((Data?) -> Void)) {

        Alamofire.request( url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { response in
            switch response.result {
            case .success :
                if let resultJson = response.data{
                    print(resultJson)
                    completion(resultJson)
                    // Success
                    
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
    
    public func getRequestParam(url: String, param: [String: Any] , completion: @escaping((Data?) -> Void)) {

        Alamofire.request( url, method: .get, parameters: param, encoding: URLEncoding.queryString, headers: nil).responseJSON { response in
            switch response.result {
            case .success :
                if let resultJson = response.data{
                    print(resultJson)
                    completion(resultJson)
                    // Success
                    
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    public func postRequest(url: String, param: [String: Any], completion: @escaping((Data?) -> Void)) {
        Alamofire.request( url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success :
                if let resultJson = response.data{
                    print(resultJson)
                    completion(resultJson)
                    // Success
                    
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
    
    public func getRequestAuth(url: String, token: String, completion: @escaping((Data?) -> Void)) {
        let headers: HTTPHeaders = ["Authorization": "Bearer " + token]

        Alamofire.request( url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            switch response.result {
            case .success :
                if let resultJson = response.data{
                    print(resultJson)
                    completion(resultJson)
                    // Success
                    
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
    
    public func requestAuth(url: String, method: HTTPMethod , token: String, param: [String: Any], completion: @escaping((Data?) -> Void)) {
        let headers: HTTPHeaders = ["Authorization": "Bearer " + token]

        Alamofire.request( url, method: method, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success :
                if let resultJson = response.data{
                    print(resultJson)
                    completion(resultJson)
                    // Success
                    
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
}
