//
//  Request.swift
//  TJK
//
//  Created by Shatlyk on 14.07.2022.
//

import Foundation

struct CreateOrderedRequest: Codable {
    let amount: Int
    let activeSize: String
    let id: Int
}
