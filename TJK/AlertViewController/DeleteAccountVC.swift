//
//  DeleteAccountVC.swift
//  TJK
//
//  Created by Shatlyk on 05.10.2022.
//

import UIKit
import Alamofire

class DeleteAccountVC: UIViewController {

    var myTabBarController = TabBarController()
    var mainStoryboard = UIStoryboard()
    let helperDb = HelperDB()
    let apiCall = ApiCall()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var deleteBtn: LocalizableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.alpha = 0.0
        
        mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        myTabBarController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController

        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func deleteAction(_ sender: Any) {
        self.activityIndicator.alpha = 1.0
        self.activityIndicator.startAnimating()
        self.deleteBtn.alpha = 0.0
        self.deleteBtn.isEnabled = false
        print("Basyldy delete")
        
        guard let token = self.helperDb.getToken() else { return }
        let headers: HTTPHeaders = ["Authorization": "Bearer " + token.access]

        Alamofire.request( URLString.DELETE_USER, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success :
                if let resultJson = response.data{
                    print(resultJson)
                    // Success
                    DispatchQueue.main.async {
                        self.helperDb.removeUser()
                        self.helperDb.removeToken()
                        let defaults = UserDefaults.standard
                        defaults.removeObject(forKey: "Favorite")
                        self.view.window?.rootViewController = self.myTabBarController
                        
                    }
                    
                }
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self.activityIndicator.alpha = 0.0
                    self.activityIndicator.stopAnimating()
                    self.deleteBtn.alpha = 1.0
                    self.deleteBtn.isEnabled = true
                }
            }
        }
        
        

    }
}
