//
//  Utils.swift
//  TJK
//
//  Created by Shatlyk on 04.07.2022.
//

import Foundation
import UIKit

 class Utils {
    public func setShadow(_ view: UIView){
        let color = UIColor.black
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 5
     
   }
    public func setBottomShadow(_ view: UIView){
        let color = UIColor.black
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 5
        view.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: view.bounds.maxY - view.layer.shadowRadius, width: view.bounds.width, height: view.layer.shadowRadius), cornerRadius: view.layer.cornerRadius).cgPath
     
   }
}
//MARK: html teksti Stringe gecirya
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension UIViewController {
    func showToast(message : String, font: UIFont) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - self.view.frame.size.width/4, y: self.view.frame.size.height-100, width: self.view.frame.width/2, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 1.0, delay: 2.0, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
