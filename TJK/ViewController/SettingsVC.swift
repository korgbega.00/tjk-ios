//
//  SettingsVC.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class SettingsVC: UIViewController {

    var mainStoryboard = UIStoryboard()
    var myTabBarController = TabBarController()
    var aboutVC = AboutVC()
    let helperDb = HelperDB()
    
    @IBOutlet weak var logoutButton: LocalizableButton!
    @IBOutlet weak var removeAccauntBtn: UIButton!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var heightLogoutButton: NSLayoutConstraint!
    @IBOutlet weak var heightRemoveAccauntBtn: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        myTabBarController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        
        if helperDb.getUser() != nil {
            logoutButton.alpha = 1.0
            heightLogoutButton.constant = 40
            
            removeAccauntBtn.alpha = 1.0
            heightRemoveAccauntBtn.constant = 40
        } else {
            logoutButton.alpha = 0.0
            heightLogoutButton.constant = 0
            
            removeAccauntBtn.alpha = 0.0
            heightRemoveAccauntBtn.constant = 0
        }

//        if let bundleIdentifier = Bundle.main.bundleIdentifier {
//            appSupportURL.appendingPathComponent("\(bundleIdentifier)").appendingPathComponent("Documents")
//        }
        
//        let bundleIdentifier =  Bundle.main.bundleIdentifier
//          appSupportURL.appendingPathComponent("\(bundleIdentifier)").appendingPathComponent("Documents")
//        
        
        //get app version
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        if let version = appVersion {
            appVersionLabel.text = version
        }
        
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        print("Settings dismiss")
    }
    
    @IBAction func rateAppAction(_ sender: Any) {
        
    }
    
    @IBAction func shareAction(_ sender: Any) {
        let textToShare = [NSURL(string: "https://stackoverflow.com/questions/35931946/basic-example-for-sharing-text-or-image-with-uiactivityviewcontroller-in-swift")]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view

        self.present(activityViewController, animated: true, completion: nil)

    }
    @IBAction func howToOrderAction(_ sender: Any) {
        aboutVC = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        aboutVC.nameFunc = "howToOrder"
        present(aboutVC, animated: true, completion: nil)
    }
    
    @IBAction func paymentOrderAction(_ sender: Any) {
        aboutVC = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        aboutVC.nameFunc = "paymentAndDeliveryProcedure"
        present(aboutVC, animated: true, completion: nil)

    }
    
    @IBAction func aboutUsAction(_ sender: Any) {
        aboutVC = mainStoryboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        aboutVC.nameFunc = "aboutUs"
        present(aboutVC, animated: true, completion: nil)

    }
    
    @IBAction func logoutAction(_ sender: Any) {
        helperDb.removeUser()
        helperDb.removeToken()
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "Favorite")
        self.view.window?.rootViewController = self.myTabBarController
        
    }
    @IBAction func removeAccauntAction(_ sender: Any) {
        var setAppKeyAlertController : DeleteAccountVC!
        
        setAppKeyAlertController = mainStoryboard.instantiateViewController(withIdentifier: "DeleteAccountVC") as? DeleteAccountVC
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(setAppKeyAlertController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)
    }
}
