//
//  PopularGoodsViewController.swift
//  TJK
//
//  Created by Shatlyk on 20.06.2022.
//

import UIKit

class PopularGoodsVC: UIViewController {
    
    let utils = Utils()
    var slug = "slug"
    var titleTK = ""
    var titleRU = ""
    var titleEN = ""
    let identifier = "PopularGoodsCell"
    var customStoryboard = UIStoryboard()
    var popularGoodsDetailVC = PopularGoodsDetailVC()
    let apiCall = ApiCall()
    var productResultsArray = [ProductResults]()
    var whiteHeartImage = UIImage()
    var heartImage = UIImage()
    let defaults = UserDefaults.standard
    let helperDB = HelperDB()
    var currentLanguage = "ru"
    let refreshControl = UIRefreshControl()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightConsCollectionView: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    
        whiteHeartImage = UIImage(named: "heart_black") ?? UIImage()
        heartImage = UIImage(named: "heart") ?? UIImage()
        
        collectionView.delegate = self
        collectionView.dataSource  = self
        
        //customize UI
        scrollView.bounces = true
//        scrollView.refreshControl = UIRefreshControl()
//        scrollView.refreshControl?.tintColor = UIColor.black
        refreshControl.transform = CGAffineTransform(scaleX: 0.9, y: 0.9);
        refreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        collectionView.isScrollEnabled = false
        
        call()
        
    }
    
    @objc func handleRefreshControl() {
        call()
    }
    
    private func call() {
        apiCall.getRequest(url: URLString.URL_PRODUCTS) { productsJson in
            guard let productsJson = productsJson else {
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                }
                print("productsJson = nil")
                return
                
            }
            
            let productResponse: ProductResponse = try! JSONDecoder().decode(ProductResponse.self, from: productsJson)
            print("productResponse.count = \(productResponse.count)")
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            self.filterData(productResponse: productResponse)
            
            if (self.productResultsArray.count % 2) == 0 {
                self.heightConsCollectionView.constant = CGFloat((self.productResultsArray.count/2 * 317) + 22 + 20 + ((self.productResultsArray.count/2 - 1) * 10))
            } else {
                self.heightConsCollectionView.constant = CGFloat(((self.productResultsArray.count/2 + 1) * 317) + 22 + 20 + ((self.productResultsArray.count/2) * 10))
            }
            
            self.collectionView.reloadData()
        }
    }
    
    private func filterData(productResponse: ProductResponse){
        for i in (0 ..< (productResponse.count)) {
            if productResponse.results[i].category.slug == slug {
                productResultsArray.append(productResponse.results[i])
                
            }
        }
    }

}
extension PopularGoodsVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productResultsArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier , for: indexPath) as! PopularGoodsCell
        
        //customize UI
        cell.layer.masksToBounds = false
        utils.setShadow(cell)
        cell.widthAnchor.constraint(equalToConstant: (self.view.frame.width-50)/2).isActive = true
        
        //download image
        self.apiCall.getData(from: URL(string: productResultsArray[indexPath.item].images[0].image)!) { data, response, error in
                guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                cell.imageView.image = UIImage(data: data)
            }
        }
        
        //set data
        if currentLanguage == "tk" {
            cell.nameLabel.text = productResultsArray[indexPath.item].title

        } else if currentLanguage == "ru" {
            cell.nameLabel.text = productResultsArray[indexPath.item].title_ru

        } else if currentLanguage == "en" {
            cell.nameLabel.text = productResultsArray[indexPath.item].title_en

        }
        
        cell.amountLabel.text = "\(productResultsArray[indexPath.item].price) TMT"
        let favorites = helperDB.getFavorite()
        cell.favoriteButton.setImage(self.heartImage, for: .normal)
        
        for favorite in favorites {
            if productResultsArray[indexPath.item].id == favorite.id {
                cell.favoriteButton.setImage(whiteHeartImage, for: .normal)
                break
            }
        }
        cell.addFavoritePressed = {
            
            let param : [String : Any] = [
                "product_id" : self.productResultsArray[indexPath.item].id]
            
            guard let token = self.helperDB.getToken() else { return }
            
            if cell.favoriteButton.image(for: .normal) == self.heartImage {
            self.apiCall.requestAuth(url: URLString.URL_ADD_FAVORITE, method: .post, token: token.access, param: param) { data in
                
                guard let data = data else { return }
                let status: Status = try! JSONDecoder().decode(Status.self, from: data)
                
                if status.status == "success" {
                    cell.favoriteButton.setImage(self.whiteHeartImage, for: .normal)
                    self.helperDB.addFavorite(Favorite(id: self.productResultsArray[indexPath.item].id))
                    NotificationCenter.default.post(name: NSNotification.Name("FAVORITE"), object: nil)
                }
            }} else if cell.favoriteButton.image(for: .normal) == self.whiteHeartImage {
                self.apiCall.requestAuth(url: URLString.URL_ADD_FAVORITE, method: .delete, token: token.access, param: param) { data in
                    
                    guard let data = data else { return }
                    let status: Status = try! JSONDecoder().decode(Status.self, from: data)
                    var favorites2 = self.helperDB.getFavorite()
                    
                    if status.status == "success" {
                        for i in (0 ..< favorites2.count) {
                            if self.productResultsArray[indexPath.item].id == favorites2[i].id {
                                favorites2.remove(at: i )
                                cell.favoriteButton.setImage(self.heartImage, for: .normal)
                                self.helperDB.setFavorite(favorites2)
    
                                NotificationCenter.default.post(name: NSNotification.Name("FAVORITE"), object: nil)
                                break
                            }
                        }
                    }
                }
                
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        popularGoodsDetailVC = customStoryboard.instantiateViewController(withIdentifier: String(describing: PopularGoodsDetailVC.self)) as! PopularGoodsDetailVC
        
        popularGoodsDetailVC.productResults = self.productResultsArray[indexPath.item]
        popularGoodsDetailVC.productResultsArray = self.productResultsArray
        
        present(popularGoodsDetailVC, animated: true, completion: nil)
        
    }
    
}

