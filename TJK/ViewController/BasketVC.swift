//
//  BasketVC.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class BasketVC: UIViewController {

    let utils = Utils()
    let cellBackgroundView = UIView()
    var buttonColor = UIColor()
    var customStoryboard = UIStoryboard()
    var confimirationVC = ConfimirationVC()
    var minusButton = UIButton()
    var plusButton = UIButton()
    var orders = [Order]()
    let helperDB = HelperDB()
    let apiCall = ApiCall()
    var shippingMethod = ""
    var currentLanguage = "ru"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightConsTableView: NSLayoutConstraint!
    @IBOutlet weak var ashgabatCityButton: UIButton!
    @IBOutlet weak var otherCityButton: UIButton!
    @IBOutlet weak var actionBar: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var orderCountLabel: UILabel!
    @IBOutlet weak var orderPriceLabel: UILabel!
    @IBOutlet weak var allPriceLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
                
        //init varnings
        orders = helperDB.getOrders()
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        confimirationVC = customStoryboard.instantiateViewController(withIdentifier: "ConfimirationVC") as! ConfimirationVC
        
        tableView.dataSource = self
        tableView.delegate = self
        
        //customize UI
        tableView.bounces = false
        scrollView.bounces = false
        tableView.showsVerticalScrollIndicator = false
        cellBackgroundView.backgroundColor = UIColor.white
        
        //notification center 
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDataVC(notification:)), name: NSNotification.Name("BASKET"), object: nil)
        
        //The color RGB #175F8F
        buttonColor = UIColor(red: CGFloat(0x17)/255
                              ,green: CGFloat(0x5F)/255
                              ,blue: CGFloat(0x8F)/255
                              ,alpha: 1.0)
        heightConsTableView.constant = CGFloat(170 * orders.count)
        tableView.isScrollEnabled = true
        utils.setBottomShadow(actionBar)
        if self.currentLanguage == "tk" {
            self.countLabel.text = "\(self.orders.count) Haryt"

        } else if self.currentLanguage == "ru" {
            self.countLabel.text = "\(self.orders.count) Товары"

        } else if self.currentLanguage == "en" {
            self.countLabel.text = "\(self.orders.count) Haryt"
            
        }
        orderCountLabel.text = "\(self.orders.count)"
        orderPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: shippingMethod)[0])"
        allPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: shippingMethod)[1])"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    private func calculator(_ orders: [Order], shippingMethod: String) -> [Float] {
        var allPrice = Float()
        if shippingMethod == "asgabat" {
            for order in orders {
                allPrice += (order.price) * Float(order.count)
            }
            allPrice += 15
            return [allPrice - 15, allPrice]
        } else if shippingMethod == "welayat" {
            for order in orders {
                allPrice += (order.price) * Float(order.count)
            }
            allPrice += 50
            return [allPrice - 50, allPrice]
        } else {
            for order in orders {
                allPrice += (order.price) * Float(order.count)
            }
            return [allPrice, allPrice]
        }
    }
    
    @objc func reloadDataVC(notification: Notification) {
        orders = helperDB.getOrders()
        self.heightConsTableView.constant = CGFloat(170 * self.orders.count)
        tableView.reloadData()
        
        if self.currentLanguage == "tk" {
            self.countLabel.text = "\(self.orders.count) Haryt"

        } else if self.currentLanguage == "ru" {
            self.countLabel.text = "\(self.orders.count) Товары"

        } else if self.currentLanguage == "en" {
            self.countLabel.text = "\(self.orders.count) Haryt"
            
        }
        
    }
    
    @IBAction func ashgabatCityButtonTap(_ sender: Any) {
        
        ashgabatCityButton.setImage(UIImage(named: "circle_black"), for: .normal)
        otherCityButton.setImage(UIImage(named: "circle"), for: .normal)
        
        ashgabatCityButton.tintColor = buttonColor
        otherCityButton.tintColor = .black
        shippingMethod = "asgabat"
        self.orderCountLabel.text = "\(self.orders.count)"
        self.orderPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[0])"
        self.allPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[1])"
    }
    
    @IBAction func otherCityButtonTap(_sender: Any) {            otherCityButton.setImage(UIImage(named: "circle_black"), for: .normal)
        ashgabatCityButton.setImage(UIImage(named: "circle"), for: .normal)

        otherCityButton.tintColor = buttonColor
        ashgabatCityButton.tintColor = .black
        shippingMethod = "welayat"
        self.orderCountLabel.text = "\(self.orders.count)"
        self.orderPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[0])"
        self.allPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[1])"
        
    }
    @IBAction func orederButtonTap(_ sender: Any) {
        confimirationVC.shippingMethod = shippingMethod
        confimirationVC.allPrice = calculator(orders, shippingMethod: shippingMethod)[1]
        confimirationVC.productCount = orders.count
        confimirationVC.allProductPrice = calculator(orders, shippingMethod: shippingMethod)[0]
        
        if shippingMethod == "" {
            
            if currentLanguage == "tk" {
                showToast(message: "Gowşuryş ýerini saýlaň", font: .systemFont(ofSize: 14.0))

            } else if currentLanguage == "ru" {
                showToast(message: "Выберите место доставки", font: .systemFont(ofSize: 14.0))

            } else if currentLanguage == "en" {
                showToast(message: "Choose a delivery location", font: .systemFont(ofSize: 14.0))
            }
            return
            
        } else if self.orders.isEmpty {
            
            if currentLanguage == "tk" {
                showToast(message: "Sebet boş", font: .systemFont(ofSize: 14.0))

            } else if currentLanguage == "ru" {
                showToast(message: "Корзина пусто", font: .systemFont(ofSize: 14.0))

            } else if currentLanguage == "en" {
                showToast(message: "Basket is empty", font: .systemFont(ofSize: 14.0))
            }
            return
        }
        confimirationVC.orders = self.orders
        present(confimirationVC, animated: true, completion: nil)
    
    }
}

extension BasketVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasketCell") as! BasketCell
        cell.selectedBackgroundView = self.cellBackgroundView
        
        //set data
        if currentLanguage == "tk" {
            cell.nameLabel.text = "\(self.orders[indexPath.row].name)"
            cell.sizeLabel.text = "Ölçegi: \(self.orders[indexPath.row].size)"
            
        } else if currentLanguage == "ru" {
            cell.nameLabel.text = "\(self.orders[indexPath.row].name_ru)"
            cell.sizeLabel.text = "Размер: \(self.orders[indexPath.row].size)"
            
        } else if currentLanguage == "en" {
            cell.nameLabel.text = "\(self.orders[indexPath.row].name_en)"
            cell.sizeLabel.text = "Size: \(self.orders[indexPath.row].size)"
            
        }
        
        cell.productCountLabel.text = "\(self.helperDB.getOrders()[indexPath.row].count)"
        cell.amountLabel.text = "\(self.orders[indexPath.row].price) TMT"
        
        //download image
        self.apiCall.getData(from: URL(string: orders[indexPath.row].image)!) { data, response, error in
                guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                cell.myImage.image = UIImage(data: data)
            }
        }
        
        cell.plusButtonPressed = {
            self.orders = self.helperDB.getOrders()
            if self.orders[indexPath.row].count < 10 {
                self.orders[indexPath.row].count += 1
                self.helperDB.setOrders(self.orders)
                cell.productCountLabel.text = "\(self.helperDB.getOrders()[indexPath.row].count)"
                self.orderCountLabel.text = "\(self.orders.count)"
                self.orderPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[0])"
                self.allPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[1])"
            }
        }
        
        
        cell.minusButtonPressed = {
            self.orders = self.helperDB.getOrders()
            if self.orders[indexPath.row].count > 1 {
                self.orders[indexPath.row].count -= 1
                self.helperDB.setOrders(self.orders)
                cell.productCountLabel.text = "\(self.helperDB.getOrders()[indexPath.row].count)"
                self.orderCountLabel.text = "\(self.orders.count)"
                self.orderPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[0])"
                self.allPriceLabel.text = "\(self.calculator(self.orders, shippingMethod: self.shippingMethod)[1])"
            } else if self.orders[indexPath.row].count == 1 {
                self.orders.remove(at: indexPath.row)
                self.helperDB.setOrders(self.orders)
                self.countLabel.text = "0 Haryt"
                self.heightConsTableView.constant = CGFloat(170 * self.orders.count)
                self.tableView.reloadData()
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
