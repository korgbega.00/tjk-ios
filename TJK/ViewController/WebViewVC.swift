//
//  WebViewVC.swift
//  TJK
//
//  Created by Shatlyk on 14.07.2022.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {

    var urlString = ""
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let myURL = URL(string: urlString )
        let myRequest = URLRequest(url: myURL!)
        webView.loadRequest(myRequest)

    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
