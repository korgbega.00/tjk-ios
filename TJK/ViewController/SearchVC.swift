//
//  SearchVC.swift
//  TJK
//
//  Created by Shatlyk on 18.07.2022.
//

import UIKit

class SearchVC: UIViewController {

    var searchText = ""
    let utils = Utils()
    let apiCall = ApiCall()
    let helperDB = HelperDB()
    var mainStoryboard = UIStoryboard()
    var popularGoodsDetailVC = PopularGoodsDetailVC()
    let identifier = "PopularGoodsCell"
    var productResultsArray = [ProductResults]()
    var whiteHeartImage = UIImage()
    var heartImage = UIImage()
    var currentLanguage = "ru"

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var actionBar: UIView!
    @IBOutlet weak var searchLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        whiteHeartImage = UIImage(named: "heart_black") ?? UIImage()
        heartImage = UIImage(named: "heart") ?? UIImage()

        collectionView.delegate = self
        collectionView.dataSource = self
        mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //customize UI
        if currentLanguage == "tk" {
            searchLabel.text = "Gözleg: \(searchText)"

        } else if currentLanguage == "ru" {
            searchLabel.text = "Поиск: \(searchText)"


        } else if currentLanguage == "en" {
            searchLabel.text = "Search: \(searchText)"

        }
        collectionView.bounces = false
        
        let todosEndpoint: String = URLString.URL_PRODUCTS_SEARCH + "?search=\(searchText)"

        apiCall.getRequest(url: todosEndpoint) { data in
            
            guard let data = data else { print("data = nil"); return }

            let productResponse: ProductResponse = try! JSONDecoder().decode(ProductResponse.self, from: data)
            if productResponse.count == 0 {
                DispatchQueue.main.async {
                    if self.currentLanguage == "tk" {
                        self.searchLabel.text = "Tapylmady: \(self.searchText)"

                    } else if self.currentLanguage == "ru" {
                        self.searchLabel.text = "Ничего не найдено: \(self.searchText)"


                    } else if self.currentLanguage == "en" {
                        self.searchLabel.text = "Nothing found: \(self.searchText)"

                    }
                }

            }
            
            self.productResultsArray = productResponse.results
            self.collectionView.reloadData()
        }
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productResultsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier , for: indexPath) as! PopularGoodsCell
        
        //customize UI
        cell.layer.masksToBounds = false
        utils.setShadow(cell)
        cell.widthAnchor.constraint(equalToConstant: (self.view.frame.width-50)/2).isActive = true
        
        //download image
        self.apiCall.getData(from: URL(string: productResultsArray[indexPath.item].images[0].image)!) { data, response, error in
                guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                cell.imageView.image = UIImage(data: data)
            }
        }
        
        //set data
        if currentLanguage == "tk" {
            cell.nameLabel.text = productResultsArray[indexPath.item].title

        } else if currentLanguage == "ru" {
            cell.nameLabel.text = productResultsArray[indexPath.item].title_ru


        } else if currentLanguage == "en" {
            cell.nameLabel.text = productResultsArray[indexPath.item].title_en

        }
        cell.amountLabel.text = "\(productResultsArray[indexPath.item].price) TMT"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        popularGoodsDetailVC = mainStoryboard.instantiateViewController(withIdentifier: String(describing: PopularGoodsDetailVC.self)) as! PopularGoodsDetailVC
        popularGoodsDetailVC.productResults = self.productResultsArray[indexPath.item]
        popularGoodsDetailVC.productResultsArray = self.productResultsArray
        present(popularGoodsDetailVC, animated: true, completion: nil)
        
    }

}
