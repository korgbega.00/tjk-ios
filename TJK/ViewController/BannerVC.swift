//
//  BannerVC.swift
//  TJK
//
//  Created by Shatlyk on 26.06.2022.
//

import UIKit

class BannerVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    var image = UIImage()
    var customStryboard = UIStoryboard()
    var vc = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customStryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        vc = customStryboard.instantiateViewController(withIdentifier: "BannerVC") as! BannerVC
        
        
        imageView.image = image
     
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        vc.dismiss(animated: true, completion: nil)
    }
}
