//
//  ConfimirationVC.swift
//  TJK
//
//  Created by Shatlyk on 10.07.2022.
//

import UIKit

class ConfimirationVC: UIViewController {

    var productCount = 0
    var allProductPrice = Float()
    var shippingMethod = ""
    var allPrice = Float()
    let helperDB  = HelperDB()
    let apiCall = ApiCall()
    var buttonColor = UIColor()
    var userDB = UserDB(id: 0, username: "", email: "", account: nil, first_name: "", last_name: "")
    var paymentMethod = ""
    var orders = [Order]()
    var items = [[String: Any]]()
    var mainStoryboard = UIStoryboard()
    var webViewVC = WebViewVC()
    var currentLanguage = "ru"

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addRessTextField: UITextField!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var dosLabel: UILabel!
    @IBOutlet weak var allLabel: UILabel!
    @IBOutlet weak var pay1Button: UIButton!
    @IBOutlet weak var pay2Button: UIButton!
    @IBOutlet weak var pay3Button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        webViewVC = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        
        if let user = helperDB.getUser() {
            self.userDB = user
            print(user)
            nameTextField.text = user.first_name
            emailTextField.text = user.email
            addRessTextField.text = user.account?.address
            phoneTextField.text = user.account?.phone
        }
        productCountLabel.text = "\(productCount)"
        productPriceLabel.text = "\(allProductPrice)"
        allLabel.text = "\(allPrice)"
        
        if shippingMethod == "asgabat" {
            dosLabel.text = "15"

        } else if shippingMethod == "welayat"  {
            dosLabel.text = "50"
        }
        
        //customize UI
        scrollView.bounces = false
        scrollView.showsVerticalScrollIndicator = false
        buttonColor = UIColor(red: CGFloat(0x17)/255
                              ,green: CGFloat(0x5F)/255
                              ,blue: CGFloat(0x8F)/255
                              ,alpha: 1.0)
        

        //create items
        for i in (0 ..< orders.count) {
            var item = [String: Any]()
            
            item["amount"] = orders[i].count
            item["activeSize"] = orders[i].size
            item["id"] = orders[i].id
            items.append(item)
        }
        
        print(items)
        
    }
    
    private func toast(){
        if currentLanguage == "tk" {
            self.showToast(message: "Ählisini dolduryň", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "ru" {
            self.showToast(message: "Заполните все поля", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "en" {
            self.showToast(message: "Fill in all", font: .systemFont(ofSize: 14.0))
        }
        
    }
    
    @IBAction func pay1ButtonTap(_ sender: Any) {
        pay1Button.setImage(UIImage(named: "circle_black"), for: .normal)
        pay2Button.setImage(UIImage(named: "circle"), for: .normal)
        pay3Button.setImage(UIImage(named: "circle"), for: .normal)
      
        pay1Button.tintColor = buttonColor
        pay2Button.tintColor = .black
        pay3Button.tintColor = .black

        paymentMethod = "nagt"

    }
    
    @IBAction func pay2ButtonTap(_ sender: Any) {
        pay2Button.setImage(UIImage(named: "circle_black"), for: .normal)
        pay1Button.setImage(UIImage(named: "circle"), for: .normal)
        pay3Button.setImage(UIImage(named: "circle"), for: .normal)

        pay2Button.tintColor = buttonColor
        pay1Button.tintColor = .black
        pay3Button.tintColor = .black
        paymentMethod = "baranda-cart"
    }
    
    @IBAction func pay3ButtonTap(_ sender: Any) {
        pay3Button.setImage(UIImage(named: "circle_black"), for: .normal)
        pay2Button.setImage(UIImage(named: "circle"), for: .normal)
        pay1Button.setImage(UIImage(named: "circle"), for: .normal)

        pay3Button.tintColor = buttonColor
        pay1Button.tintColor = .black
        pay2Button.tintColor = .black
        paymentMethod = "cart"
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func orderedButtonTap(_ sender: Any) {
        guard let nameText = nameTextField.text else { toast(); return }
        guard let phoneText = phoneTextField.text else { toast(); return }
        guard let emailText = emailTextField.text else { toast(); return }
        guard let addRessText = addRessTextField.text else { toast(); return }
        
        if (nameText == "") || (phoneText == "") || (emailText == "") || (addRessText == "") || shippingMethod == "" {
            toast()
            return
    
        } else if paymentMethod == "" {
            if currentLanguage == "tk" {
                showToast(message: "Töleg usulyny saýlaň", font: .systemFont(ofSize: 14.0))

            } else if currentLanguage == "ru" {
                showToast(message: "Выберите способ оплаты", font: .systemFont(ofSize: 14.0))

            } else if currentLanguage == "en" {
                showToast(message: "Select a Payment Method", font: .systemFont(ofSize: 14.0))
            }
            
        } else {
  
        let param : [String : Any] = [
            "address" : addRessText,
            "email" : emailText,
            "name" : nameText,
            "payment_method" : paymentMethod,
            "phone" : phoneText,
            "shipping_method" : shippingMethod,
            "items": items
        ]

        if let token = helperDB.getToken() {
            
            apiCall.requestAuth(url: URLString.URL_CREATE_ORDER, method: .post , token: token.access, param: param) { data in
                guard let data = data else { print("data = nil"); return }
                
                //payment method equal "cart"
                if self.paymentMethod == "cart" {
           
                let orderResponseCard: OrderResponseCard = try! JSONDecoder().decode(OrderResponseCard.self, from: data)
                print("orderResponse.status = \(orderResponseCard.status)")
                print("orderResponse.url = \(orderResponseCard.url)")

                if orderResponseCard.status == "online" {
                    self.webViewVC.urlString = orderResponseCard.url
                    self.present(self.webViewVC, animated: true, completion: nil)
                
                } else {
                    self.showToast(message: "Basha barmady", font: .systemFont(ofSize: 14.0))
                }
            } else {
               //code payment method equal "nagt" and "baranda-cart"
                
                let orderResponse: OrderResponse = try! JSONDecoder().decode(OrderResponse.self, from: data)
                print("orderResponse.status = \(orderResponse.status)")
                
                self.showToast(message: "\(orderResponse.status)", font: .systemFont(ofSize: 14.0))
            }
            
            }} else {
            //code not token
                apiCall.postRequest(url: URLString.URL_CREATE_ORDER, param: param) { data in
                    guard let data = data else { print("data = nil"); return }
                    
                    //payment method equal "cart"
                    if self.paymentMethod == "cart" {
                        let orderResponseCard: OrderResponseCard = try! JSONDecoder().decode(OrderResponseCard.self, from: data)
                        print("orderResponse.status = \(orderResponseCard.status)")
                        print("orderResponse.url = \(orderResponseCard.url)")

                        if orderResponseCard.status == "online" {
                            self.webViewVC.urlString = orderResponseCard.url
                            self.present(self.webViewVC, animated: true, completion: nil)
                            
                        } else {
                            if self.currentLanguage == "tk" {
                                self.showToast(message: "Başa barmady", font: .systemFont(ofSize: 14.0))

                            } else if self.currentLanguage == "ru" {
                                self.showToast(message: "Не получилось", font: .systemFont(ofSize: 14.0))

                            } else if self.currentLanguage == "en" {
                                self.showToast(message: "Did not work out", font: .systemFont(ofSize: 14.0))
                            }                        }
                        
                    } else {
                        //code payment method equal "nagt" and "baranda-cart"
                         
                        let orderResponse: OrderResponse = try! JSONDecoder().decode(OrderResponse.self, from: data)
                        print("orderResponse.status = \(orderResponse.status)")
                         
                        self.showToast(message: "\(orderResponse.status)", font: .systemFont(ofSize: 14.0))
                    }
                }
            }
        
        }
    
    }
    
}
