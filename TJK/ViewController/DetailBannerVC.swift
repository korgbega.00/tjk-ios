//
//  DetailBannerVC.swift
//  TJK
//
//  Created by Shatlyk on 08.07.2022.
//

import UIKit

class DetailBannerVC: UIViewController {

    var urlImage = String()
    let apiCall = ApiCall()
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.apiCall.getData(from: URL(string: urlImage)!) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: data)
                print("Gelo = \(response?.suggestedFilename as Any)")

            }
            
        }
    }
}
