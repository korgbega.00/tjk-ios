//
//  AboutVC.swift
//  TJK
//
//  Created by Shatlyk on 10.09.2022.
//

import UIKit

class AboutVC: UIViewController {
    
    var nameFunc = ""
    let apiCall = ApiCall()
    let helperDB = HelperDB()
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var customTitle: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        //customize UI
        textView.showsVerticalScrollIndicator = false
        self.customTitle.text = ""

        
        if nameFunc == "howToOrder" {
            howToOrder()
        } else if nameFunc == "paymentAndDeliveryProcedure" {
            paymentAndDeliveryProcedure()
        } else if nameFunc == "aboutUs" {
            aboutUs()
        }
        
    }
    
    
    func howToOrder() {
        apiCall.getRequest(url: URLString.URL_ABOUT) { data in
            guard let data = data else {
                print("data = nil")
                return
            }
            let aboutUsResponse: AboutUsResponse = try! JSONDecoder().decode(AboutUsResponse.self, from: data)
            
            DispatchQueue.main.async {
                if self.currentLanguage == "tk" {
                    self.customTitle.text = "Nädip sargyt etmeli"
                    self.textView.text = aboutUsResponse.results[0].description.htmlToString

                } else if self.currentLanguage == "ru" {
                    self.customTitle.text = "Kак заказать"
                    self.textView.text = aboutUsResponse.results[0].description_ru.htmlToString

                } else if self.currentLanguage == "en" {
                    self.customTitle.text = "How to order"
                    self.textView.text = aboutUsResponse.results[0].description_en.htmlToString

                }
            }
            
        }
    }
    
    func paymentAndDeliveryProcedure() {
        apiCall.getRequest(url: URLString.URL_ABOUT) { data in
            guard let data = data else {
                print("data = nil")
                return
            }
            let aboutUsResponse: AboutUsResponse = try! JSONDecoder().decode(AboutUsResponse.self, from: data)
            
            DispatchQueue.main.async {
                if self.currentLanguage == "tk" {
                    self.customTitle.text = "Töleg tertibi we eltip bermek"
                    self.textView.text = aboutUsResponse.results[1].description.htmlToString

                } else if self.currentLanguage == "ru" {
                    self.customTitle.text = "Способы оплаты и условия доставки"
                    self.textView.text = aboutUsResponse.results[1].description_ru.htmlToString

                } else if self.currentLanguage == "en" {
                    self.customTitle.text = "Payment and delivery terms"
                    self.textView.text = aboutUsResponse.results[1].description_en.htmlToString

                }
            }
            
        }

    }
    
    func aboutUs() {
        apiCall.getRequest(url: URLString.URL_ABOUT) { data in
            guard let data = data else {
                print("data = nil")
                return
            }
            let aboutUsResponse: AboutUsResponse = try! JSONDecoder().decode(AboutUsResponse.self, from: data)
            
            DispatchQueue.main.async {
                if self.currentLanguage == "tk" {
                    self.customTitle.text = "Türkmenbaşy jins toplumy"
                    self.textView.text = aboutUsResponse.results[2].description.htmlToString

                } else if self.currentLanguage == "ru" {
                    self.customTitle.text = "Джинсовый комплекс Туркменбаши"
                    self.textView.text = aboutUsResponse.results[2].description_ru.htmlToString

                } else if self.currentLanguage == "en" {
                    self.customTitle.text = "Turkmenbashi Jeans Complex"
                    self.textView.text = aboutUsResponse.results[2].description_en.htmlToString

                }
            }
        }
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
