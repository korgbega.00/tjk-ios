//
//  LanguageVC.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class LanguageVC: UIViewController {

    let defaults = UserDefaults.standard
    var myTabBarController = TabBarController()
    var mainStoryboard = UIStoryboard()
  
    override func viewDidLoad() {
        super.viewDidLoad()

        mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        myTabBarController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
       
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Turkmen(_ sender: Any) {
        defaults.set("tk", forKey: "CurrentLanguage")
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        self.view.window?.rootViewController = self.myTabBarController

    }
    
    @IBAction func Russian(_ sender: Any) {
        defaults.set("ru", forKey: "CurrentLanguage")
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        self.view.window?.rootViewController = self.myTabBarController

    }
    
    @IBAction func English(_ sender: Any) {
        defaults.set("en", forKey: "CurrentLanguage")
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        self.view.window?.rootViewController = self.myTabBarController

    }
    
}
