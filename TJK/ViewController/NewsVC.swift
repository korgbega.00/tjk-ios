//
//  NewsVC.swift
//  TJK
//
//  Created by Shatlyk on 29.06.2022.
//

import UIKit

class NewsVC: UIViewController {

    var customStoryBoard = UIStoryboard()
    var newsDetailVC = NewsDetailVC()
    let apiCall = ApiCall()
    var newsResponse = NewsResponse(count: 0, results: [NewsResults.init(id: 0, title: "", title_ru: "", title_en: "", description: "", description_ru: "", description_en: "", image: "", created_at: "", view: 0)])
    var imageArray = [UIImage]()
    var currentLanguage = "ru"
    let refreashControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        customStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
//        tableView.refreshControl = UIRefreshControl()
//        scrollView.refreshControl?.tintColor = UIColor.black
        refreashControl.transform = CGAffineTransform(scaleX: 0.9, y: 0.9);
        refreashControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        tableView.addSubview(refreashControl)
        call()
    }
    
    @objc func handleRefreshControl() {
        call()
    }
    
    private func call(){
        apiCall.getRequest(url: URLString.URL_NEWS) { newsJson in
            guard let newsJson = newsJson else {
                DispatchQueue.main.async {
                    self.refreashControl.endRefreshing()
                }
                
                print("newsJson = nil")
                return
                
            }
            
            let newsResponse: NewsResponse = try! JSONDecoder().decode(NewsResponse.self, from: newsJson)
            self.refreashControl.endRefreshing()
            print("newsData.count = \(newsResponse.count)")
            self.newsResponse = newsResponse

            for i in (0 ..< (newsResponse.count)) {
                
                self.newsResponse.results[i].description = newsResponse.results[i].description.htmlToString
                self.newsResponse.results[i].description_en = newsResponse.results[i].description_en.htmlToString
                self.newsResponse.results[i].description_ru = newsResponse.results[i].description_ru.htmlToString
                
                if i == newsResponse.count - 1 {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension NewsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        
        //download image
        self.apiCall.getData(from: URL(string: newsResponse.results[indexPath.row].image)!) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                cell.newsImageView.image = UIImage(data: data)

            }
        }
        
        //set data
        if currentLanguage == "tk" {
            cell.title.text = newsResponse.results[indexPath.row].title
            cell.descrLabel.text = newsResponse.results[indexPath.row].description

        } else if currentLanguage == "ru" {
            cell.title.text = newsResponse.results[indexPath.row].title_ru
            cell.descrLabel.text = newsResponse.results[indexPath.row].description_ru

        } else if currentLanguage == "en" {
            cell.title.text = newsResponse.results[indexPath.row].title_en
            cell.descrLabel.text = newsResponse.results[indexPath.row].description_en

        }
        
        cell.date.text = newsResponse.results[indexPath.row].created_at
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(indexPath.row)
        newsDetailVC = customStoryBoard.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
       
        if currentLanguage == "tk" {
            newsDetailVC.customTitle = newsResponse.results[indexPath.row].title
            newsDetailVC.descrip = newsResponse.results[indexPath.row].description
     
        } else if currentLanguage == "ru" {
            newsDetailVC.customTitle = newsResponse.results[indexPath.row].title_ru
            newsDetailVC.descrip = newsResponse.results[indexPath.row].description_ru
     
        } else if currentLanguage == "en" {
            newsDetailVC.customTitle = newsResponse.results[indexPath.row].title_en
            newsDetailVC.descrip = newsResponse.results[indexPath.row].description_en
        }
        
        newsDetailVC.date = newsResponse.results[indexPath.row].created_at
        newsDetailVC.viewCount = newsResponse.results[indexPath.row].view
        newsDetailVC.urlImage = newsResponse.results[indexPath.row].image
        
        present(newsDetailVC, animated: true, completion: nil)
        
    }
}

