//
//  FavoriteVC.swift
//  TJK
//
//  Created by Shatlyk on 01.07.2022.
//

import UIKit

class FavoriteVC: UIViewController {

    let utils = Utils()
    let apiCall = ApiCall()
    let helperDB = HelperDB()
    var getFavoriteResponse = GetFavoriteResponse(count: 0, results: [GetFavoriteResponseResults(product: ProductResults(id: 0, title: "", title_ru: "", title_en: "", price: 0.0, category: ProductResultsCategory(slug: ""), sizes: [ProductResultsSizes(title: "", value: 0)], images: [ProductResultsImages(image: "")]))])
    var token = ""
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    
    @IBOutlet weak var actionBar: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.bounces = false
        utils.setBottomShadow(actionBar)
       
        //notification
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDataVC(_:)), name: NSNotification.Name("FAVORITE"), object: nil)
        
        if let fToken = helperDB.getToken() {
            token = fToken.access
        }
        
        apiCall.getRequestAuth(url: URLString.URL_GET_FAVORITE, token: token) { favJson in
            guard let favJson = favJson else { print("favJson = nil"); return }
            
            let getFavoriteResponse: GetFavoriteResponse = try! JSONDecoder().decode(GetFavoriteResponse.self, from: favJson)
            
            var favorites = [Favorite]()
            for result in getFavoriteResponse.results {
                favorites.append(Favorite(id: result.product.id))
            }
            self.helperDB.setFavorite(favorites)
            
            self.getFavoriteResponse = getFavoriteResponse
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                if self.currentLanguage == "tk" {
                    self.countLabel.text = "\(getFavoriteResponse.count) Haryt"

                } else if self.currentLanguage == "ru" {
                    self.countLabel.text = "\(getFavoriteResponse.count) Товары"

                } else if self.currentLanguage == "en" {
                    self.countLabel.text = "\(getFavoriteResponse.count) Haryt"
                }
                
            }
        }
        
    }
    
    @objc func reloadDataVC(_ notification: Notification) {
        if let fToken = helperDB.getToken() {
            token = fToken.access
        }
        
        apiCall.getRequestAuth(url: URLString.URL_GET_FAVORITE, token: token) { favJson in
            guard let favJson = favJson else { print("favJson = nil"); return }
            
            let getFavoriteResponse: GetFavoriteResponse = try! JSONDecoder().decode(GetFavoriteResponse.self, from: favJson)
            
            var favorites = [Favorite]()
            for result in getFavoriteResponse.results {
                favorites.append(Favorite(id: result.product.id))
            }
            self.helperDB.setFavorite(favorites)
            
            self.getFavoriteResponse = getFavoriteResponse
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                if self.currentLanguage == "tk" {
                    self.countLabel.text = "\(getFavoriteResponse.count) Haryt"

                } else if self.currentLanguage == "ru" {
                    self.countLabel.text = "\(getFavoriteResponse.count) Товары"

                } else if self.currentLanguage == "en" {
                    self.countLabel.text = "\(getFavoriteResponse.count) Haryt"
                    
                }
            }
            
        }
        
    }

}

extension FavoriteVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if getFavoriteResponse.count == 1 {
            return 2
        }
        
        return getFavoriteResponse.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
        if (getFavoriteResponse.count == 1) && (indexPath.item == 1) {
            cell.contentView.alpha = 0
            return cell
        } else {
            cell.contentView.alpha = 1.0
        }
        
        
        //customize UI
        cell.layer.masksToBounds = false
        utils.setShadow(cell)
        cell.widthAnchor.constraint(equalToConstant: (self.view.frame.width-50)/2).isActive = true
        
        //set data
        if self.currentLanguage == "tk" {
            cell.nameLabel.text = getFavoriteResponse.results[indexPath.item].product.title

        } else if self.currentLanguage == "ru" {
            cell.nameLabel.text = getFavoriteResponse.results[indexPath.item].product.title_ru

        } else if self.currentLanguage == "en" {
            cell.nameLabel.text = getFavoriteResponse.results[indexPath.item].product.title_en
        }
        
        cell.priceLabel.text = "\(getFavoriteResponse.results[indexPath.item].product.price) TMT"
        
        //download image
        apiCall.getData(from: URL(string: getFavoriteResponse.results[indexPath.item].product.images[0].image)!) { data, response, error in
            guard let data = data else { return }
            
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                cell.fImageVeiw.image = image
            }
        }
        
        cell.removePressed = {
            
                let param : [String : Any] = [
                    "product_id" : self.getFavoriteResponse.results[indexPath.item].product.id]
            
            self.apiCall.requestAuth(url: URLString.URL_ADD_FAVORITE, method: .delete, token: self.token, param: param) { data in
                
                guard let data = data else { return }
                let status: Status = try! JSONDecoder().decode(Status.self, from: data)
                
                if status.status == "success" {
                    self.getFavoriteResponse.results.remove(at: indexPath.item)
                    self.getFavoriteResponse.count -= 1
                    self.collectionView.reloadData()
                    DispatchQueue.main.async {
                        self.showToast(message: "\(status.status)", font: .systemFont(ofSize: 14.0))
                        if self.currentLanguage == "tk" {
                            self.countLabel.text = "\(self.getFavoriteResponse.count) Haryt"

                        } else if self.currentLanguage == "ru" {
                            self.countLabel.text = "\(self.getFavoriteResponse.count) Товары"

                        } else if self.currentLanguage == "en" {
                            self.countLabel.text = "\(self.getFavoriteResponse.count) Haryt"
                        }
                        
                        var favorites = [Favorite]()
                        for results in self.getFavoriteResponse.results {
                            favorites.append(Favorite(id: results.product.id))
                        }
                        self.helperDB.setFavorite(favorites)
                    }

                }

                
            }
        }
        
        return cell
    }
}
