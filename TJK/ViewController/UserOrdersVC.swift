//
//  UserOrdersVC.swift
//  TJK
//
//  Created by Shatlyk on 19.09.2022.
//

import UIKit

class UserOrdersVC: UIViewController {

    let apiCall = ApiCall()
    var myOrders: MyOrdersResponse?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        apiCall.getRequestParam(url: URLString.MY_ORDERS, param: ["user" : 93]) { data in
            guard let data = data else {
                print("data = nil")
                return
                
            }
            
            do {
                let myOrdersResponse: MyOrdersResponse = try JSONDecoder().decode(MyOrdersResponse.self, from: data)
                print("MyOrdersResponse = \(myOrdersResponse.results)")

                DispatchQueue.main.async {
                    self.myOrders = myOrdersResponse
                    self.tableView.reloadData()
                }
                
            } catch{
                print(error)
            }
        }

    }
    
    func formatDate(date: String) -> String {
       let dateFormatterGet = DateFormatter()
       dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MM/dd/yyyy"

       let dateObj: Date? = dateFormatterGet.date(from: date)

       return dateFormatterPrint.string(from: dateObj!)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension UserOrdersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let myOrders = self.myOrders else {
            return 0
        }
        
        return myOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserOrdersCell") as! UserOrdersCell
        
        guard let myOrders = self.myOrders else { return cell }
        
        cell.idLabel.text = "\(myOrders.results[indexPath.row].id)"
        cell.regionLabel.text = "\(myOrders.results[indexPath.row].shipping_method)"
        cell.amountLabel.text = "\(myOrders.results[indexPath.row].total)"

        let date = formatDate(date: "2022-07-15T03:00:44.003940+05:00")
        
        cell.dateLabel.text = "\(date)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
}

extension Date {
    static func getFormattedDate(string: String , formatter:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        let date: Date? = dateFormatterGet.date(from: "2018-02-01T19:10:04+00:00")
        print("Date",dateFormatterPrint.string(from: date!)) // Feb 01,2018
        return dateFormatterPrint.string(from: date!);
    }
}
