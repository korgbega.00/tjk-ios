//
//  RegisterVC.swift
//  TJK
//
//  Created by Shatlyk on 24.06.2022.
//

import UIKit

class RegisterVC: UIViewController {
    
    let apiCall = ApiCall()
    let helperDB = HelperDB()
    var myTabBarController = TabBarController()
    var customStoryboard = UIStoryboard()
    var currentLanguage = "ru"
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        //init
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        myTabBarController = customStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        
        nameTextField.delegate = self
        emailTextField.delegate = self
        addressTextField.delegate = self
        phoneTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerButtonTap(_ sender: Any) {
        guard let emailText = emailTextField.text else { toast(); return }
        guard let nameText = nameTextField.text else { toast(); return }
        guard let passwordText = passwordTextField.text else { toast(); return }
        guard let phoneText = phoneTextField.text else { toast(); return }
        guard let addressText = addressTextField.text else { toast(); return }

        if (emailText == "") || (nameText == "") || (passwordText == "") || (phoneText == "") || (addressText == "") {
            self.toast()
            return
        }
        
            let param : [String : Any] = [
                "username" : emailText,
                "email" : emailText,
                "first_name" : nameText,
                "password" : passwordText,
                "phone" : phoneText,
                "address" : addressText]
                    
            apiCall.postRequest(url: URLString.URL_REGISTER, param: param) { responseJson in
                guard let json = responseJson else { print("json = nil"); return }

                do {
                let registerResponse: RegisterResponse = try JSONDecoder().decode(RegisterResponse.self, from: json)

                print(registerResponse.user)
                if registerResponse.success == "true" {
                    
                    let defaults = UserDefaults.standard
                    let accountDB = AccountDB(id: registerResponse.user.account.id, address: registerResponse.user.account.address, phone: registerResponse.user.account.phone)
                    
                    let userDB = UserDB(id: registerResponse.user.id, username: registerResponse.user.username, email: registerResponse.user.email, account:accountDB , first_name: registerResponse.user.first_name, last_name: "")
                    
                    do {
                        let encoder = JSONEncoder()
                        let data = try encoder.encode(userDB)
                        defaults.set(data, forKey: "User")
                        
                    } catch {
                        print(error)
                        self.showToast(message: "Basha barmady", font: .systemFont(ofSize: 14.0))
                    }
                    
                    let param2 : [String : Any] = [
                        "username" : emailText,
                        "password" : passwordText]
                    
                    self.apiCall.postRequest(url: URLString.URL_TOKEN, param: param2) { tokenJson in
                        guard let json2 = tokenJson else { print("json2 = nil"); return }
                        
                        do {
                        let token: Token = try JSONDecoder().decode(Token.self, from: json2)
                            print("token = \(token)")
                            
                            //save token
                            let myToken = MyToken(refresh: token.refresh , access: token.access)
                            
                            do {
                                let encoder = JSONEncoder()
                                let data2 = try encoder.encode(myToken)
                                defaults.set(data2, forKey: "MyToken")
                                
                                guard let _ = self.helperDB.getToken() else {
                                    self.toastSecond()
                                    return
                                }
                                //update app
                                self.view.window?.rootViewController = self.myTabBarController
                                
                            } catch {
                                print(error)
                                self.toastSecond()
                                
                            }
                            
                        } catch (let error2) {
                            print(error2)
                            self.toastSecond()
                        }
                    }
                    
                } else {
                    self.toastSecond()
                }
                } catch ( let error) {
                    print("error= \(error)")
                    DispatchQueue.main.async {
                        if self.currentLanguage == "tk" {
                            self.showToast(message: "Hasaba alynan", font: .systemFont(ofSize: 14.0))

                        } else if self.currentLanguage == "ru" {
                            self.showToast(message: "Уже зарегестрированы", font: .systemFont(ofSize: 14.0))

                        } else if self.currentLanguage == "en" {
                            self.showToast(message: "Already registered", font: .systemFont(ofSize: 14.0))
                        }
                    }
                }
            }

    }
    
    private func toast(){
        if currentLanguage == "tk" {
            self.showToast(message: "Ählisini dolduryň", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "ru" {
            self.showToast(message: "Заполните все поля", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "en" {
            self.showToast(message: "Fill in all", font: .systemFont(ofSize: 14.0))
        }
        
    }
    
    private func toastSecond(){
        if currentLanguage == "tk" {
            self.showToast(message: "Başa barmady", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "ru" {
            self.showToast(message: "Не получилось", font: .systemFont(ofSize: 14.0))

        } else if currentLanguage == "en" {
            self.showToast(message: "Did not work out", font: .systemFont(ofSize: 14.0))
        }
    }

}

extension RegisterVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let newString = (text as NSString).replacingCharacters(in: range, with: string)

            if textField == nameTextField {
                if  newString.count > 100 {
                    return false
                }
            }
            
            if textField == emailTextField {
                if  newString.count > 50 {
                    return false
                }
            }
            
            if textField == addressTextField {
                if  newString.count > 100 {
                    return false
                }
            }
            
            if textField == phoneTextField {
                if  newString.count > 8 {
                    return false
                }
            }
            
            if textField == passwordTextField {
                if  newString.count > 20 {
                    return false
                }
            }
            
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
