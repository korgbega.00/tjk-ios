//
//  NewsDetailVC.swift
//  TJK
//
//  Created by Shatlyk on 29.06.2022.
//

import UIKit

class NewsDetailVC: UIViewController {

    var urlImage = String()
    var descrip = String()
    var customTitle = String()
    var viewCount = Int()
    var date = String()
    let apiCall = ApiCall()
    
    @IBOutlet weak var heightConsBanner: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //customize UI
        scrollView.bounces = false
        heightConsBanner.constant = self.view.frame.height/3
        
        //download image
        self.apiCall.getData(from: URL(string: urlImage)!) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: data)

            }
        }
    
        //set data
        titleLabel.text = customTitle
        dateLabel.text = date
        viewCountLabel.text = "\(viewCount)"
        descriptionLabel.text = descrip
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
