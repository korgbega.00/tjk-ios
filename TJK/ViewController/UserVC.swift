//
//  UserVC.swift
//  TJK
//
//  Created by Shatlyk on 19.07.2022.
//

import UIKit

class UserVC: UIViewController {
    
    let helperDB = HelperDB()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = helperDB.getUser()
        
        nameLabel.text = user?.first_name
        phoneLabel.text = user?.account?.phone
        addressLabel.text = user?.account?.address
        
    }
}
