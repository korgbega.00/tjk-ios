//
//  ViewController.swift
//  TJK
//
//  Created by Shatlyk on 16.06.2022.
//

import UIKit
import Alamofire

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}

class MainVC: UIViewController {

    var embeddedViewController: MainPVC!
    var mainStoryboard = UIStoryboard()
    var searchVC = SearchVC()
    var timer = Timer()
    
    @IBOutlet weak var womenButton: UIButton!
    @IBOutlet weak var manButton: UIButton!
    @IBOutlet weak var kidsButton: UIButton!
    @IBOutlet weak var lineView : UIView!
    @IBOutlet weak var rightConsLineView: NSLayoutConstraint!
    @IBOutlet weak var witdhConsLineView: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var noNetworkView: UIView!

    @IBOutlet weak var logoView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        searchTextField.delegate = self
        
        witdhConsLineView.constant = self.view.frame.width / 3
        
        manButton.addTarget(self, action: #selector(manButtonTapped), for: .touchUpInside)
        womenButton.addTarget(self, action: #selector(womenButtonTapped), for: .touchUpInside)
        kidsButton.addTarget(self, action: #selector(kidsButtonTapped), for: .touchUpInside)
     
        self.tabBarController?.tabBar.alpha = 0.0
        
        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.TimerFunc), userInfo: nil, repeats: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MainPVC,
           segue.identifier == "EmbedSegue" {
            vc.mainVCDelegate = self
            self.embeddedViewController = vc
        }
    }
    
    @objc func TimerFunc(){
        self.tabBarController?.tabBar.alpha = 1.0
        self.logoView.alpha = 0.0
        self.timer.invalidate()
    }
    
    @objc func womenButtonTapped() {
        embeddedViewController.setVC(index: 0)
            UIView.animate(withDuration: 0.3) {
                self.rightConsLineView.constant = self.witdhConsLineView.constant * 0
                self.view.layoutIfNeeded()
        }
    }
    
    @objc func manButtonTapped() {
        embeddedViewController.setVC(index: 1)
            UIView.animate(withDuration: 0.3) {
                self.rightConsLineView.constant = self.witdhConsLineView.constant * 1
                self.view.layoutIfNeeded()
        }
        
}
    @objc func kidsButtonTapped() {
        embeddedViewController.setVC(index: 2)
            UIView.animate(withDuration: 0.3) {
                self.rightConsLineView.constant = self.witdhConsLineView.constant * 2
                self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func search(_ sender: Any) {
        guard let searchText = searchTextField.text else { return }
        if searchText != "" {
            searchVC = mainStoryboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
            searchVC.searchText = searchText
            present(searchVC, animated: true, completion: nil)
        }
    }
    @IBAction func reload(_ sender: Any) {
        if Connectivity.isConnectedToInternet {
             print("Connected")
            let myTabBarController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        
            self.noNetworkView.alpha = 0.0
            self.view.window?.rootViewController = myTabBarController
         } else {
             print("No Internet")
        }

        
    }
    
}

protocol MainVCDelegate: AnyObject {
    func changeButtonTitle(index: Int, title: String)
    func noNetIt(_ bool : Bool )
}

extension MainVC: MainVCDelegate {
    func noNetIt(_ bool: Bool) {
        if bool {
            noNetworkView.alpha = 1.0
        } else {
            noNetworkView.alpha = 0.0
        }
    }

    
    func changeButtonTitle(index: Int, title: String) {
        if index == 0 {
            self.womenButton.setTitle(title.uppercased(), for: .normal)
        }
        
        if index == 1 {
            self.manButton.setTitle(title.uppercased(), for: .normal)
        }
        
        if index == 2 {
            self.kidsButton.setTitle(title.uppercased(), for: .normal)
        }
    }
    
}
extension MainVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
