//
//  ProfilViewController.swift
//  TJK
//
//  Created by Shatlyk on 17.06.2022.
//

import UIKit

class ProfilVC: UIViewController {

    var customStoryboard = UIStoryboard()
    var settingsVC = UIViewController()
    var embeddedViewController: ProfilPVC!
    let helperDB = HelperDB()
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var lineView : UIView!
    @IBOutlet weak var rightConsLineView: NSLayoutConstraint!
    @IBOutlet weak var witdhConsLineView: NSLayoutConstraint!
    @IBOutlet weak var heightConsStackView: NSLayoutConstraint!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        settingsVC = customStoryboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        
        if helperDB.getUser() != nil {
            emailLabel.text = helperDB.getUser()?.email
        }
        
        witdhConsLineView.constant = self.view.frame.width / 2
        
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
        
        if helperDB.getUser() != nil {
            self.heightConsStackView.constant = 0
            lineView.alpha = 0.0
            loginButton.alpha = 0.0
            registerButton.alpha = 0.0
        } else {
            self.heightConsStackView.constant = 32
            lineView.alpha = 1.0
            loginButton.alpha = 1.0
            registerButton.alpha = 1.0
        }
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProfilPVC,
           segue.identifier == "EmbedSegueSecond" {
            self.embeddedViewController = vc
            vc.profilVCDelegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func loginButtonTapped() {
        embeddedViewController.setVC(index: 0)
            UIView.animate(withDuration: 0.3) {
                self.rightConsLineView.constant = self.witdhConsLineView.constant * 0
                self.view.layoutIfNeeded()
        }
    }
    
    @objc func registerButtonTapped() {
        embeddedViewController.setVC(index: 1)
            UIView.animate(withDuration: 0.3) {
                self.rightConsLineView.constant = self.witdhConsLineView.constant * 1
                self.view.layoutIfNeeded()
        }
        
}
    @IBAction func pushSettings(_ sender: Any) {
        present(settingsVC, animated: true, completion: nil)
    }
}

protocol ProfilVCDelegate: AnyObject {
    func changeConsZero()
    func changeConsOne()
}

extension ProfilVC: ProfilVCDelegate {
    func changeConsZero() {
        UIView.animate(withDuration: 0.3) {
            self.rightConsLineView.constant = self.witdhConsLineView.constant * 0
            self.view.layoutIfNeeded()
            print("Zero")
        }
    }
    
    func changeConsOne() {
        UIView.animate(withDuration: 0.3) {
            self.rightConsLineView.constant = self.witdhConsLineView.constant * 1
            self.view.layoutIfNeeded()
            print("One")
        }
    }
    
}
