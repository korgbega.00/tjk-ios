//
//  PopularGoodsDetailVC.swift
//  TJK
//
//  Created by Shatlyk on 28.06.2022.
//

import UIKit

class PopularGoodsDetailVC: UIViewController {
    
    let utils = Utils()
    var productResults = ProductResults(id: 0, title: "", title_ru: "", title_en: "", price: 0, category: ProductResultsCategory(slug: ""), sizes: [ProductResultsSizes(title: "", value: 0)], images: [ProductResultsImages(image: "")])
    var productResultsArray = [ProductResults]()
    var embeddedViewController: BannerPopularGoodsDetailPVC!
    let apiCall = ApiCall()
    var customStoryboard = UIStoryboard()
    var defaults = UserDefaults.standard
    var orders = [Order]()
    let helperDB = HelperDB()
    var productCount = 1
    var sizeValue : String?
    var currentLanguage = "ru"
    
    @IBOutlet weak var sizeCollectionView: UICollectionView!
    @IBOutlet weak var recomendedCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var productCountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let language = UserDefaults.standard.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        customStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        sizeCollectionView.delegate = self
        sizeCollectionView.dataSource = self
        recomendedCollectionView.delegate = self
        recomendedCollectionView.dataSource = self
        
        //customize UI
        recomendedCollectionView.bounces = false
        sizeCollectionView.bounces = false
        scrollView.bounces = false
        sizeCollectionView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        recomendedCollectionView.showsHorizontalScrollIndicator = false
        
        //set data
        if currentLanguage == "tk" {
            nameLabel.text = productResults.title
        } else if currentLanguage == "ru" {
            nameLabel.text = productResults.title_ru

        } else if currentLanguage == "en" {
            nameLabel.text = productResults.title_en
        }
        
        amountLabel.text = "\(productResults.price) TMT"
        productCountLabel.text = "\(productCount)"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? BannerPopularGoodsDetailPVC,
           segue.identifier == "EmbedSegueBPGDPVC" {
            self.embeddedViewController = vc
            self.embeddedViewController.productResultsImagesArray = self.productResults.images
    
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func basketButtonTap(_ sender: Any) {
        let basketVC =  self.customStoryboard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        present(basketVC, animated: true, completion: nil)
    }
    
    @IBAction func minusButtonTap(_ sender: Any) {
        if productCount > 1 {
            productCount -= 1
            productCountLabel.text = "\(productCount)"
        }
    }
    
    @IBAction func plusButtonTap(_ sender: Any) {
        if productCount < 10 {
            productCount += 1
            productCountLabel.text = "\(productCount)"
        }
    }
    
    @IBAction func addBasket(_ sender: Any) {
        if let sizeValue = self.sizeValue {
            let order = Order(id: productResults.id, count: productCount, size: sizeValue, image: productResults.images[0].image, name: productResults.title, name_ru: productResults.title_ru, name_en: productResults.title_en, price: productResults.price)
            
            helperDB.addOrder(order)
            
            NotificationCenter.default.post(name: NSNotification.Name("BASKET"), object: nil)
                        
            if currentLanguage == "tk" {
                showToast(message: "Sebede goşuldy", font: UIFont.systemFont(ofSize: 14.0))
                
            } else if currentLanguage == "ru" {
                showToast(message: "Добавлено", font: UIFont.systemFont(ofSize: 14.0))

            } else if currentLanguage == "en" {
                showToast(message: "Added to cart", font: UIFont.systemFont(ofSize: 14.0))
            }
            
            print(helperDB.getOrders())
            
        } else {
            if currentLanguage == "tk" {
                showToast(message: "Ölçegini saýlaň", font: UIFont.systemFont(ofSize: 14.0))
                
            } else if currentLanguage == "ru" {
                showToast(message: "Выберите размер", font: UIFont.systemFont(ofSize: 14.0))

            } else if currentLanguage == "en" {
                showToast(message: "Select size", font: UIFont.systemFont(ofSize: 14.0))
            }
            
        }

    }
    
}

extension PopularGoodsDetailVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == sizeCollectionView {
            return productResults.sizes.count
        }
        if collectionView == recomendedCollectionView {
            return productResultsArray.count
        }
        return Int()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sizeCell = sizeCollectionView.dequeueReusableCell(withReuseIdentifier: "SizeCell", for: indexPath) as! SizeCell
        let recomendedCell = recomendedCollectionView.dequeueReusableCell(withReuseIdentifier: "DetailCell", for: indexPath) as! DetailCell
        
        //size collectionView
        if collectionView == sizeCollectionView {
            //set data
            sizeCell.sizeLabel.text = "\(self.productResults.sizes[indexPath.row].title)"
            if let sizeValue = self.sizeValue {
                if sizeValue == productResults.sizes[indexPath.row].title {
                    sizeCell.sizeLabel.textColor = .black
                } else {
                    //ACACAC
                    sizeCell.sizeLabel.textColor = UIColor(red: CGFloat(0xAC)/255
                                                           ,green: CGFloat(0xAC)/255
                                                           ,blue: CGFloat(0xAC)/255
                                                           ,alpha: 1.0)
                }
            }
            
            return sizeCell
        }
        
        //recomended collectionView
        if collectionView == recomendedCollectionView {
            //customize UI
            recomendedCell.layer.masksToBounds = false
            utils.setShadow(recomendedCell)
                        
            //download image
            self.apiCall.getData(from: URL(string: productResultsArray[indexPath.row].images[0].image)!) { data, response, error in
                    guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    recomendedCell.imageView.image = UIImage(data: data)
                }
            }
            
            //set data
            if currentLanguage == "tk" {
                recomendedCell.nameLabel.text = productResultsArray[indexPath.row].title
            } else if currentLanguage == "ru" {
                recomendedCell.nameLabel.text = productResultsArray[indexPath.row].title_ru

            } else if currentLanguage == "en" {
                recomendedCell.nameLabel.text = productResultsArray[indexPath.row].title_en
            }
            recomendedCell.amountLabel.text = "\(productResultsArray[indexPath.row].price)"
            
            recomendedCell.sizeLabel.text = ""
            for size in productResultsArray[indexPath.row].sizes {
                guard let sizeLabelText = recomendedCell.sizeLabel.text else { return UICollectionViewCell() }
                recomendedCell.sizeLabel.text = "\(sizeLabelText)\(size.title), "
            }
            
            
            return recomendedCell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == sizeCollectionView {
            return CGSize(width: 50, height:   17)
        }
        if collectionView == recomendedCollectionView {
            return CGSize(width: 191, height: 340)
        }
        return CGSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView == sizeCollectionView {
            self.sizeValue = self.productResults.sizes[indexPath.row].title
            collectionView.reloadData()
        }
        
//        if collectionView == recomendedCollectionView {
//            var popularGoodsDetailVC = PopularGoodsDetailVC()
//
//            popularGoodsDetailVC = customStoryboard.instantiateViewController(withIdentifier: String(describing: PopularGoodsDetailVC.self)) as! PopularGoodsDetailVC
//
//            popularGoodsDetailVC.productResults = self.productResultsArray[indexPath.item]
//            popularGoodsDetailVC.productResultsArray = self.productResultsArray
//
//            present(popularGoodsDetailVC, animated: true, completion: nil)
//        }
    }
    
}
